CREATE SCHEMA IF NOT EXISTS tienda DEFAULT CHARACTER SET utf8mb4;
USE tienda;

DROP TABLE IF EXISTS OpcionesMeses;
DROP TABLE IF EXISTS ArchivosProcesados;

CREATE TABLE OpcionesMeses(
    opcionId int(11) NOT NULL AUTO_INCREMENT,
    prefijoTarjeta int,
    institutcionBancaria varchar(20),
    tipoTarjeta varchar (1),
    nombreBanco varchar (50),
    status int(1),
    tipoPago varchar (1),
    monto3Meses int,
    monto6Meses int,
    monto12Meses int,
    creadoEn Date,
    cargaArchivo int,
    exchangeId varchar(100),
    PRIMARY KEY (opcionId)
);

CREATE TABLE ArchivosProcesados (
    idArchivo int,
    nombre varchar(100),
    createdAt Date,
    registrosProcesados int,
    exchangeId varchar(100)
);

insert into OpcionesMeses(prefijoTarjeta, institutcionBancaria, tipoTarjeta,
nombreBanco, status, tipoPago, monto3Meses, monto6Meses, monto12Meses, creadoEn, cargaArchivo, exchangeId)
values(1, 'banamex', '1','banamex', 1, 'C', 300, 600, 1200, now(), 1, 'exchange001');

insert into OpcionesMeses(prefijoTarjeta, institutcionBancaria, tipoTarjeta,
nombreBanco, status, tipoPago, monto3Meses, monto6Meses, monto12Meses, creadoEn, cargaArchivo, exchangeId)
values(12, 'Bancomer BBVA', '1','Bancomer BBVA', 1, 'C', 3000, 6000, 12000, now(), 0, 'exchange000');