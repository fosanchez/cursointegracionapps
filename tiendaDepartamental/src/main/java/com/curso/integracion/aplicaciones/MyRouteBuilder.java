package com.curso.integracion.aplicaciones;

import org.apache.camel.builder.RouteBuilder;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends RouteBuilder {

    public void configure() {
      from("direct:load1").log("load1").delay(3000);
      from("direct:load2").log("load2").delay(4000);
      from("direct:load3").log("load3").delay(1000);
      from("direct:load4").log("load4").delay(3000);
      from("direct:load5").log("load5").delay(3500);
    }

}
