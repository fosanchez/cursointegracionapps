package com.curso.integracion.aplicaciones.entity;

import java.util.Date;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import lombok.Data;

@Data
public class ArchivosProcesados {

  @DataField(pos = 1)
  private int idArchivo;

  @DataField(pos = 2)
  private String nombre;

  @DataField(pos = 3)
  private Date createdAt;

  @DataField(pos = 4)
  private int registrosProcesados;

  @DataField(pos = 5)
  private int exchangeId;

}
