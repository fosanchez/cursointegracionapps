package com.curso.integracion.aplicaciones.routes;

import java.sql.SQLNonTransientConnectionException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.curso.integracion.aplicaciones.entity.OpcionesMeses;

@Service
public class PagoService extends RouteBuilder {

  @Autowired
  DataSource datasource;

  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

  @SuppressWarnings("unchecked")
  @Override
  public void configure() throws Exception {
    onException(NullPointerException.class, SQLNonTransientConnectionException.class)
        .process((ex) -> {
          System.out.println("handling ex");
        }).log("Received body").handled(true).transform()
        .simple("Error reported: ${exception.message} - cannot process this message.").end();
    from("direct:insert").process(this::save).to("jdbc:dataSource");
    from("direct:findById").process(this::queryById).to("jdbc:dataSource").process(this::getData);
    from("direct:finAll").to("sql:select * from tienda.OpcionesMeses").process(this::getData);
    from("direct:finAllFiles").to("sql:select * from tienda.OpcionesMeses where cargaArchivo = 0")
        .process(this::getData);
    from("direct:edit").process(this::editPago).to("jdbc:dataSource");
    // from("direct:delete").process(this::deletePago).to("jdbc:dataSource");
  }

  private void save(Exchange exchange) throws Exception {
    OpcionesMeses opcion = exchange.getIn().getBody(OpcionesMeses.class);
    String query = "INSERT INTO OpcionesMeses(prefijoTarjeta,institutcionBancaria,tipoTarjeta,"
        + "nombreBanco, status, tipoPago, monto3Meses, monto6Meses,monto12Meses,creadoEn,cargaArchivo,"
        + "exchangeId) VALUES(" + opcion.getPrefijoTarjeta() + ",'"
        + opcion.getInstitutcionBancaria() + "','" + opcion.getTipoTarjeta() + "','"
        + opcion.getNombreBanco() + "'," + opcion.getStatus() + ",'" + opcion.getTipoPago() + "',"
        + opcion.getMonto3Meses() + "," + opcion.getMonto6Meses() + "," + opcion.getMonto12Meses()
        + ",'" + dtf.format(LocalDateTime.now()) + "',0,'" + opcion.getExchangeId() + "')";
    exchange.getIn().setBody(query);
  }

  private void editPago(Exchange exchange) throws Exception {
    OpcionesMeses opcion = exchange.getIn().getBody(OpcionesMeses.class);
    String query = "UPDATE OpcionesMeses set prefijoTarjeta=" + opcion.getPrefijoTarjeta()
        + ",institutcionBancaria='" + opcion.getInstitutcionBancaria() + "',tipoTarjeta='"
        + opcion.getTipoTarjeta() + "',nombreBanco='" + opcion.getNombreBanco() + "', status="
        + opcion.getStatus() + ", tipoPago='" + opcion.getTipoPago() + "',monto3Meses="
        + opcion.getMonto3Meses() + ", monto6Meses=" + opcion.getMonto6Meses() + ", monto12Meses="
        + opcion.getMonto12Meses() + ", creadoEn='" + dtf.format(LocalDateTime.now()) + "', cargaArchivo="
        + opcion.getCargaArchivo() + ",exchangeId='" + opcion.getExchangeId() 
        + "' where opcionId="+ opcion.getOpcionId();
    exchange.getIn().setBody(query);
  }

  private void queryById(Exchange exchange) throws Exception {
    String query = "select * from tienda.OpcionesMeses where opcionId = "
        + exchange.getIn().getBody(Integer.class);
    exchange.getIn().setBody(query);
  }

  @SuppressWarnings("unchecked")
  private void getData(Exchange exchange) throws Exception {
    ArrayList<Map<String, Object>> dataList =
        (ArrayList<Map<String, Object>>) exchange.getIn().getBody();
    List<OpcionesMeses> opcionesPago = new ArrayList<OpcionesMeses>();

    for (Map<String, Object> m : dataList) {
      opcionesPago.add(new OpcionesMeses((Integer) m.get("opcionId"),
          (Integer) m.get("prefijoTarjeta"), m.get("institutcionBancaria").toString(),
          m.get("tipoTarjeta").toString(), m.get("nombreBanco").toString(),
          (Integer) m.get("status"), m.get("tipoPago").toString(), (Integer) m.get("monto3Meses"),
          (Integer) m.get("monto6Meses"), (Integer) m.get("monto12Meses"), (Date) m.get("creadoEn"),
          (Integer) m.get("cargaArchivo"), m.get("exchangeId").toString()));
    }
    exchange.getIn().setBody(opcionesPago);
  }


}
