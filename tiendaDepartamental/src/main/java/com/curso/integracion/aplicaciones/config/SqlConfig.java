package com.curso.integracion.aplicaciones.config;


import org.apache.camel.component.sql.SqlComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import javax.sql.DataSource;

public class SqlConfig {

  @Autowired
  DataSource dataSource;

  @Bean
  public SqlComponent sql(DataSource dataSource) {
    SqlComponent sql = new SqlComponent();
    sql.setDataSource(dataSource);
    return sql;
  }

}
