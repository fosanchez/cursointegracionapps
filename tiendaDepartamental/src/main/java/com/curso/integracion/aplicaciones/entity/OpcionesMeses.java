package com.curso.integracion.aplicaciones.entity;

import java.util.Date;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@CsvRecord(separator = ",")
public class OpcionesMeses {

  @DataField(pos = 1)
  private Integer opcionId;

  @DataField(pos = 2)
  private Integer prefijoTarjeta;
  
  @DataField(pos = 3)
  private String institutcionBancaria;
  
  @DataField(pos = 4)
  private String tipoTarjeta;
  
  @DataField(pos = 5)
  private String nombreBanco;
  
  @DataField(pos = 6)
  private Integer status;
  
  @DataField(pos = 7)
  private String tipoPago;
  
  @DataField(pos = 8)
  private Integer monto3Meses;

  @DataField(pos = 9)
  private Integer monto6Meses;
  
  @DataField(pos = 10)
  private Integer monto12Meses;
  
  @DataField(pos = 11)
  private Date creadoEn;
  
  @DataField(pos = 12)
  private Integer cargaArchivo; // 0 API- 1 FILE
  
  @DataField(pos = 13)
  private String exchangeId;

}
