package com.curso.integracion.aplicaciones.api;

import java.util.List;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.curso.integracion.aplicaciones.entity.OpcionesMeses;

@RestController
@RequestMapping("/opcionesPago")
public class TipoPagosController {

  @Autowired
  ProducerTemplate producerTemplate;


  @PostMapping(consumes = "application/json")
  public boolean opcionPago(@RequestBody OpcionesMeses opcion) {
    producerTemplate.requestBody("direct:insert", opcion, OpcionesMeses.class);
    return true;
  }

  @SuppressWarnings("unchecked")
  @GetMapping()
  public List<OpcionesMeses> opcionesPago() {
    List<OpcionesMeses> res =
        (List<OpcionesMeses>) producerTemplate.requestBody("direct:finAll", null, List.class);
    System.out.println(res.size());
    return res;
  }
  
  @SuppressWarnings("unchecked")
  @GetMapping(value = "/archivos")
  public List<OpcionesMeses> opcionesPagoArchivos() {
    List<OpcionesMeses> res =
        (List<OpcionesMeses>) producerTemplate.requestBody("direct:finAllFiles", null, List.class);
    System.out.println(res.size());
    return res;
  }
  
  @SuppressWarnings("unchecked")
  @GetMapping(value = "/{id}")
  public List<OpcionesMeses> opcionPago(@PathVariable(name = "id") int id) {
    return (List<OpcionesMeses>) producerTemplate.requestBody("direct:findById", id);
  }
  
  @PutMapping(consumes = "application/json")
  public boolean editOpcionPago(@RequestBody OpcionesMeses opcion){
    producerTemplate.requestBody("direct:edit", opcion, OpcionesMeses.class);
    return true;
  }
  
  @DeleteMapping(value = "/{id}")
  public boolean delOpcionPago(@PathVariable(name = "id") int id){
    producerTemplate.requestBody("direct:delete", id);
    return true;
  }
  
  /*Metodo carga de archivo*/
}
