package com.curso.integracion.aplicaciones.apache.camel.config;

import org.apache.camel.component.sql.SqlComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import javax.sql.DataSource;


public class SqlConfig {

  /*Inyeccion del Datasource configurado por el starter de jdbc*/
 @Autowired
 DataSource dataSource;

  /*
   * Configuracion del componente de Apache Camel sql
   * */
  @Bean
  public SqlComponent sql(DataSource dataSource) {
      SqlComponent sql = new SqlComponent();
      sql.setDataSource(dataSource);
      return sql;
  }

}
