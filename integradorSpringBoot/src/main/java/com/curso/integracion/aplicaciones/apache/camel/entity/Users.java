package com.curso.integracion.aplicaciones.apache.camel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@Data
@AllArgsConstructor
@CsvRecord(separator = ",")
/*Clase Users para ejemplificar el uso del componente sql y jdbc*/
public class Users {
  
  @DataField(pos = 1)
  private String userId;
  @DataField(pos = 2)
  private String userName;

}
