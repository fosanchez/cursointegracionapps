package com.curso.integracion.aplicaciones.apache.camel.api;

import com.curso.integracion.aplicaciones.apache.camel.entity.Employees;
import com.curso.integracion.aplicaciones.apache.camel.entity.Users;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import javax.annotation.PostConstruct;


/*clase rest controler para exponer ciertos endpoints que con ayuda del producerTemplate llamaran a las diferentes rutas que hemos configurado en este
 * proyecto*/
@RestController
@RequestMapping("spring")
public class UsersController {

  @Autowired
  ProducerTemplate producerTemplate;

  @GetMapping(value = "/users")
  public List<Users> getAllUsers() {
    List<Users> employees = (List<Users>) producerTemplate.requestBody("direct:findAll",null,List.class);
    System.out.println(employees.size());
    return employees;
  }
  
  @GetMapping(value = "/v2/users")
  public List<Users> getAllUsersv2() {
    List<Users> employees = (List<Users>) producerTemplate.requestBody("direct:selectSQL",null,List.class);
    System.out.println(employees.size());
    return employees;
  }

  @PostMapping(value = "/users", consumes = "application/json")
  public boolean insertUsers(@RequestBody Users user) {
    producerTemplate.requestBody("direct:insert", user, List.class);
    return true;
  }

  @GetMapping(value = "/users/{id}")
  public List<Users> getAllUsers(@PathVariable(name = "id") int id) {
    List<Users> employees = (List<Users>) producerTemplate.requestBody("direct:findById", id);
    System.out.println(employees.size());
    return employees;
  }

  @GetMapping(value = "/employees")
  public boolean getEmployees() {
    producerTemplate.requestBody("direct:processEmployee", null, Employees.class);
    return true;
  }
}
