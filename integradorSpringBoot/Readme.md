#Integracion de Aplicaciones con Apache Camel

### IntegradoSpringBoot

Proyecto Java 11 generado con el arquetipo **camel-archetype-spring-boot** en el cual se declaran ciertas rutas con el fin de demostrar el uso de diversos componentes dentro del curso **Integracion de Aplicaciones con Apache Camel**

Les adjunto las siguientes ligas como complemento.

  - [Apacahe Camel Development Guide by Red Hat](https://access.redhat.com/documentation/en-us/red_hat_fuse/7.1/html/apache_camel_development_guide/index "Apacahe Camel Development Guide") 

- [Components List by Apache Camel(Official Site)](https://camel.apache.org/components/2.x/index.html "Components List by Apache Camel(Official Site)")

- [GitHub repository of Apache Camel examples (Official Repository)](https://github.com/apache/camel-examples "GitHub repository of Apache Camel examples (Official Repository)")


-----------------

> Se utiliza un docker compose por lo cual es necesario tener instalado **Docker** y **Docker Compose** para el funcionamiento correcto de la aplicación debido a que se utilizan componentes como base de datos o active mq.

> Comando para ejecutar el docker componse
```
docker-compose up -d 
```
>   pasos para ejecutar el proyecto 
```bash
mvn clean install
mvn spring-boot:run
```